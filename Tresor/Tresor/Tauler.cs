﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Tresor
{
    class Tauler:Grid
    {
        ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(@"C:\Users\Montilivi\Desktop\Robot\RobotGit\RobotGit\bin\Debug\fondo.jpg")));

        //Direccio dir;
        int filaRobot;
        int columnaRobot;
        int nMoviments = 0;

        Imatge robot = new Imatge();
        Imatge robotCanvi = new Imatge();
        Imatge tresor = new Imatge();
        StatusBar stb = new StatusBar();
        StatusBarItem stbFletxa = new StatusBarItem();

        public int FilaRobot { get => filaRobot; set { filaRobot = value; } }
        public int ColumnaRobot { get => columnaRobot; set => columnaRobot = value; }
        public int NMoviments { get => nMoviments; set => nMoviments = value; }
        //internal Direccio Dir { get => dir; set => dir = value; }

        public void CreaJoc()
        {
            this.Children.Clear();
            this.Background = ib;

            Random r = new Random();

            stb.FontSize = 35;
            stb.Background = Brushes.Transparent;
            stbFletxa.Content = " ";
            stbFletxa.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            stb.Items.Add(stbFletxa);

            robot.Fila = r.Next(1, 5);
            robot.Columna = r.Next(0, 5);

            filaRobot = robot.Fila;
            columnaRobot = robot.Columna;

            Grid.SetRow(robot, robot.Fila);
            Grid.SetColumn(robot, robot.Columna);

            Grid.SetRow(tresor, 2);
            Grid.SetColumn(tresor, 2);

            Grid.SetRow(stb, 0);
            Grid.SetColumn(stb, 2);

            robot.Source = new BitmapImage(new Uri(@"C:\Users\Montilivi\Desktop\Robot\RobotGit\RobotGit\bin\Debug\gollum.png"));
            tresor.Source = new BitmapImage(new Uri(@"C:\Users\Montilivi\Desktop\Robot\RobotGit\RobotGit\bin\Debug\anillo.png"));

            this.Children.Add(robot);
            this.Children.Add(tresor);
            this.Children.Add(stb);
        }

        

        
    }
}
