﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Tresor
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer cronometre;

        public MainWindow()
        {
            InitializeComponent();
        }
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            #region Cronometre
            cronometre = new DispatcherTimer();
            cronometre.Interval = TimeSpan.FromSeconds(2);
            cronometre.Start();
            cronometre.Tick += Cronometre_Tick;
            #endregion

            grdTauler.CreaJoc();

        }

        private void Cronometre_Tick(object sender, EventArgs e)
        {
            //if (grdTauler.JocGuanyat())
            //{
            //    grdTauler.IsEnabled = false;
            //    cronometre.Stop();
            //    MessageBox.Show($"Gollum ha fet {grdTauler.NMoviments} moviments");
            //}

            //grdTauler.Moure();

        }
    }
}
